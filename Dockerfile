# Stage 0, "build-stage", Build using Node.js, Angular CLI. To build and compile the frontend.
FROM node:lts-alpine as build-stage

WORKDIR /src

COPY package.json package-lock.json ./

RUN npm install

COPY . .

ARG configuration=dev

RUN npm run build -- \
--configuration=$configuration \
--outputPath=dist/admin \
--baseHref=/movies/ \
--sourceMap=false \
--aot=true \
--buildOptimizer=true \
--optimization=true \
--vendorChunk=true

# Stage 1, Serve using Nginx
FROM nginx:stable

# Copy the nginx configuration file. This sets up the behavior of nginx
COPY nginx.conf /etc/nginx/nginx.conf

# create log dir configured in nginx.conf
RUN mkdir -p /var/log/app_engine

# Finally, all static assets.
COPY --from=build-stage /app/dist/ /usr/share/nginx/www/

RUN chmod -R a+r /usr/share/nginx/www
